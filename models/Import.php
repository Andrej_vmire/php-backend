<?php


namespace app\models;


use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use yii\base\Model;
use yii\web\UploadedFile;

class Import extends Model
{
    /**
     * @var UploadedFile
     */
    public $uploadedFile;

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function importing()
    {
        if (!$this->validate()) {
            return $this->getErrors();
        }
        $data = $this->getData();
        $response = [];
        foreach ($data as $val) {
            $user = [];
            foreach ($this->attributes as $key => $item) {
                $user[$key] = $val[$item];
            }
            $response[] = $user;
        }
        return $response;
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    private function getData()
    {
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);
        $sheet = $reader->load($this->uploadedFile->tempName);
        return $sheet->getActiveSheet()->toArray();
    }
}