<?php


namespace app\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * Class Departments
 * @package app\models
 *
 * @property int $id
 * @property string $name
 *
 * @property Employees[] $users
 */
class Departments extends ActiveRecord
{
    public static function tableName()
    {
        return "departments";
    }

    public function rules()
    {
        return [
            ['name', 'required']
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers(): ActiveQuery
    {
        return $this->hasMany(Employees::className(), ['id', 'departments_id']);
    }
}