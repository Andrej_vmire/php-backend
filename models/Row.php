<?php


namespace app\models;


use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;

abstract class Row extends Model
{
    public const LOGIN_LENGTH = 5;

    public const PASSWORD_LENGTH = 4;
    /**
     * @var Transliteration
     */
    protected $transliteration;

    protected $_field_separator;

    public static $row_separator = PHP_EOL;

    public function __construct($config = [])
    {
        if (!isset($this->_field_separator)) {
            throw new InvalidConfigException('$_field_separator is undefined');
        }
        $this->transliteration = Transliteration::class;
        parent::__construct($config);
    }

    /**
     * @return mixed
     */
    abstract public static function getFileName();

    public static function getArchiveName()
    {
        return '';
    }

    public function toString()
    {
        if (!$this->validate()) {
            throw new Exception(
                "Validation exception for '" . join(PHP_EOL, $this->attributes) . "'"
            );
        }
        return join($this->_field_separator, $this->attributes);
    }

    protected function increaseToMin(string $value, $len)
    {
        if (strlen($value) < $len) {
            $value = $this->increaseToMin('0' . $value, $len);
        }
        return $value;
    }

    public static function getSuperuser()
    {
        return null;
    }
}