<?php


namespace app\models;


class OldPXRow extends Row
{
    /**
     * @var string
     */
    private $_name;

    /**
     * @var string
     */
    private $_personnel_number;

    /**
     * @var string
     */
    private $_password;

    /**
     * @var string
     */
    private $_level;

    /**
     * @var string
     */
    private $_department;

    protected $_field_separator = "\t";

    public function rules()
    {
        return [
            [['name', 'personnel_number', 'password', 'level', 'department'], 'required']
        ];
    }

    public function attributes()
    {
        return [
            'name',
            'personnel_number',
            'password',
            'level',
            'department'
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $data = explode(' ', $this->transliteration::run($this->_name));
        $name = $data[0];
        $surname = isset($data[1][0]) ? $data[1][0] . '.' : '';
        $middle_name = isset($data[2][0]) ? $data[2][0] . '.' : '';
        return trim("$name $surname $middle_name");
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->_name = preg_replace("/\s{2,}/", " ", $name);
    }

    /**
     * @return string
     */
    public function getpersonnel_number(): string
    {
        return $this->increaseToMin($this->_personnel_number, self::LOGIN_LENGTH);
    }

    /**
     * @param string $personnel_number
     */
    public function setpersonnel_number(string $personnel_number): void
    {
        $this->_personnel_number = $personnel_number;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->increaseToMin($this->_password, self::PASSWORD_LENGTH);
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->_password = $password;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->_level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level): void
    {
        $this->_level = $level;
    }

    /**
     * @return string
     */
    public function getDepartment(): string
    {
        return $this->transliteration::run($this->_department);
    }

    /**
     * @param string $department
     */
    public function setDepartment(string $department): void
    {
        $this->_department = $department;
    }

    /**
     * @inheritDoc
     */
    public static function getFileName()
    {
        return 'old.txt';
    }
}