<?php


namespace App\Models;


class Transliteration
{
    static private $_table = [
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'y',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'kh',
        'ц' => 'ts',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        'ё' => 'ye',
        'ъ' => '',
        'ь' => '',
        'ы' => 'y'
    ];

    public static function run(string $data)
    {
        $result = '';
        $data = preg_split("//ui", $data, -1, PREG_SPLIT_NO_EMPTY);
        foreach ($data as $key => $value) {
            if (isset(self::$_table[$value])) {
                $result .= self::$_table[$value];
            } elseif (isset(self::$_table[mb_strtolower($value)])) {
                $result .= mb_convert_case(self::$_table[mb_strtolower($value)], MB_CASE_TITLE);
            } else {
                $result .= $value;
            }
        }
        return $result;
    }
}