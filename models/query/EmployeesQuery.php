<?php


namespace app\models\query;


use yii\db\ActiveQuery;

class EmployeesQuery extends ActiveQuery
{
    public function exceptInacive()
    {
        return $this->andWhere(['=', 'inactive', 0]);
    }

    public function withDepartment()
    {
        return $this->leftJoin('departments d', 'd.id = e.departments_id');
    }

    public function withAccess()
    {
        return $this->leftJoin('access a', 'a.id = e.access_id');
    }
}