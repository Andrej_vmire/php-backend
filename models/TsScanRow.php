<?php


namespace app\models;


class TsScanRow extends Row
{
    private $_surname;

    private $_name;

    private $_patronymic;

    private $_personnel_number;

    private $_password;

    private $_level;

    protected $_field_separator = ";";

    public function rules()
    {
        return [
            [['name', 'personnel_number', 'password', 'level'], 'required']
        ];
    }

    public function attributes()
    {
        return [
            'surname',
            'name',
            'patronymic',
            'personnel_number',
            'password',
            'level'
        ];
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->transliteration::run($this->_surname);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->transliteration::run($this->_name);
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $name = preg_replace("/\s{2,}/", " ", $name);
        $name = explode(' ', $name);
        $this->_surname = $name[0];
        $this->_name = $name[1] ?? '';
        $this->_patronymic = $name[2] ?? '';
    }

    /**
     * @return mixed
     */
    public function getPatronymic()
    {
        return $this->transliteration::run($this->_patronymic);
    }

    /**
     * @return mixed
     */
    public function getPersonnel_number()
    {
        return $this->increaseToMin($this->_personnel_number, self::LOGIN_LENGTH);
    }

    /**
     * @param mixed $personnel_number
     */
    public function setPersonnel_number($personnel_number): void
    {
        $this->_personnel_number = $personnel_number;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->increaseToMin($this->_password, self::PASSWORD_LENGTH);
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->_password = $password;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->_level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level): void
    {
        $this->_level = ($level == 1) ? 2 : 1;
    }

    /**
     * @inheritDoc
     */
    public static function getFileName()
    {
        return 'tsscan.txt';
    }
}