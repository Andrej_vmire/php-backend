<?php


namespace app\models;


use Yii;
use yii\base\Exception;

class ExportToFile extends Export
{
    protected $_root;

    protected $_file;

    public function __construct($row, $config = [])
    {
        parent::__construct($row, $config);
        $this->_root = Yii::getAlias('@files') . '/';
        $this->_file = fopen($this->_root . $this->_row::getFileName(), 'wt');
        if ($this->_file === false) {
            throw new Exception('Can not create or open file ' . $this->_row::getFileName());
        }
    }

    public function send()
    {
        $len = fwrite($this->_file, $this->_data);
        fclose($this->_file);
        if ($len !== false) {
            $file = Yii::$app->response->xSendFile('/files/' . $this->archivate(), null, ['xHeader' => 'X-Accel-Redirect']);
            $file->send();
        } else {
            throw new Exception("No data to export");
        }
    }

    private function archivate()
    {
        `cd ./files
        tar -zcvf passwd ./hiscan/
        cd ..`;
        return $this->_row::getArchiveName();
    }

}