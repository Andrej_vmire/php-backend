<?php

namespace app\models;

use app\models\query\EmployeesQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Employees
 * @package app\models
 *
 * @property int $id
 * @property int $personnel_number
 * @property string $name
 * @property int $password
 * @property int $access_id
 * @property int $departments_id
 * @property bool $inactive
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Access $access
 * @property Departments $departments
 */
class Employees extends ActiveRecord
{
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';
    public const SCENARIO_DELETE = 'delete';

    public function behaviors()
    {
        return [
            'class' => TimestampBehavior::className()
        ];
    }

    public static function tableName()
    {
        return 'employees';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['personnel_number', 'name', 'password', 'departments_id', 'access_id'];
        $scenarios[self::SCENARIO_DELETE] = [];
        $scenarios[self::SCENARIO_UPDATE] = ['personnel_number', 'name', 'password', 'departments_id', 'access_id'];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['name', 'password', 'departments_id', 'access_id'], 'required', 'on' => self::SCENARIO_CREATE],
            [['personnel_number'], 'required'],
            ['personnel_number', 'unique'],
            [['name'], 'string'],
            [['personnel_number', 'password', 'departments_id', 'access_id'], 'integer']
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAccess(): ActiveQuery
    {
        return $this->hasOne(Access::className(), ['id' => 'access_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartments(): ActiveQuery
    {
        return $this->hasOne(Departments::className(), ['id' => 'departments_id']);
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя сотрудника',
            'personnel_number' => 'Табельный №',
            'password' => 'Пароль',
            'inactive' => 'Блокировка',
            'departments_id' => 'ID подразделения',
            'access_id' => 'ID уровня допуска',
            'created_at' => 'Добавлен',
            'updated_at' => 'Отредактирован'
        ];
    }

    public static function find()
    {
        return new EmployeesQuery(get_called_class());
    }
}
