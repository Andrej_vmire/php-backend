<?php


namespace app\models;


use Yii;
use yii\web\UploadedFile;

class ImportUsers extends Import
{
    public $personnel_number;
    public $name;
    public $password;

    public function rules()
    {
        return [
            [['personnel_number', 'name', 'password'], 'required'],
            [['personnel_number', 'name', 'password'], 'integer'],
            [['uploadedFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx, xls']
        ];
    }

    public function init()
    {
        $this->attributes = Yii::$app->request->post();
        $this->uploadedFile = UploadedFile::getInstanceByName('excel_file');
    }

    public function attributes()
    {
        return [
            "personnel_number",
            "name",
            "password",
        ];
    }
}