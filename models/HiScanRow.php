<?php


namespace app\models;


class HiScanRow extends Row
{
    private $_personnel_number;

    private $_name;

    private $_password;

    private $_department;

    private $_access;

    private $_superuser_postfix;

    protected $_field_separator = PHP_EOL;

    public static $row_separator = PHP_EOL;

    public function rules()
    {
        return [
            [['personnel_number', 'name', 'password', 'department', 'access'], 'required']
        ];
    }

    public function attributes()
    {
        return [
            'personnel_number',
            'name',
            'password',
            'department',
            'access',
            'faaid',
            'superuser_postfix'
        ];
    }

    /**
     * @return string
     */
    public function getPersonnel_number()
    {
        return $this->fieldToString($this->_personnel_number, "username");
    }

    /**
     * @param string $personnel_number
     */
    public function setPersonnel_number($personnel_number): void
    {
        $this->_personnel_number = $this->increaseToMin($personnel_number, self::LOGIN_LENGTH);
    }

    protected function fieldToString(string $value, string $key = ''): string
    {
        return $key . "=" . $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->fieldToString($this->_name, 'fullname');
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->_name = $this->transliteration::run($name);
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->fieldToString($this->_password, 'password');
    }

    /**
     * @param string $password
     */
    public function setPassword($password): void
    {
        $this->_password = $this->increaseToMin($password, self::PASSWORD_LENGTH);
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->fieldToString($this->_department, 'company');
    }

    /**
     * @param string $department
     */
    public function setDepartment($department): void
    {
        $this->_department = $this->transliteration::run($department);
    }

    /**
     * @return string
     */
    public function getAccess()
    {
        return $this->fieldToString($this->_access, 'operatorid');
    }

    /**
     * @param string $access
     */
    public function setAccess($access): void
    {
        $this->_access = $access;
    }

    /**
     * @return string
     */
    public function getFaaid()
    {
        return $this->fieldToString('NONE', 'faaid');
    }

    public static function getSuperuser()
    {
        $model = new static();
        $model->_personnel_number = "004";
        $model->_name = "SERVICE";
        $model->_password = "94866";
        $model->_department = "HEIMANN SYSTEMS";
        $model->_access = "FACTORY";
        $model->_superuser_postfix = 'permanent';
        return $model;
    }

    /**
     * @return mixed
     */
    public function getSuperuser_postfix()
    {
        return $this->_superuser_postfix;
    }

    /**
     * @inheritDoc
     */
    public static function getFileName()
    {
        return 'hiscan/passwd/home/hitrax/data/passwd';
    }

    public static function getArchiveName()
    {
        return 'passwd';
    }
}