<?php


namespace app\models;


class NewPXRow extends Row
{
    /**
     * @var string
     */
    private $_surname;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var string
     */
    private $_patronymic;

    /**
     * @var string
     */
    private $_personnel_number;

    /**
     * @var string
     */
    private $_password;

    /**
     * @var string
     */
    private $_level;

    /**
     * @var string
     */
    private $_department;

    /**
     * @var string
     */
    private $_inactive;

    /**
     * @var string
     */
    private $_updated_at;

    protected $_field_separator = "\t";

    public function rules()
    {
        return [
            [['name', 'personnel_number', 'password', 'level', 'department', 'inactive', 'updated_at'], 'required']
        ];
    }

    public function attributes()
    {
        return [
            'name',
            'patronymic',
            'surname',
            'personnel_number',
            'password',
            'department',
            'level',
            'inactive',
            'updated_at'
        ];
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->transliteration::run($this->_surname);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->transliteration::run($this->_name);
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $name = preg_replace("/\./", '. ', $name);
        $name = preg_replace("/\s{2,}/", " ", $name);
        $name = explode(' ', $name);
        $this->_surname = $name[0];
        $this->_name = $name[1] ?? '';
        $this->_patronymic = $name[2] ?? '';
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->transliteration::run($this->_patronymic);
    }

    /**
     * @return string
     */
    public function getpersonnel_number()
    {
        return $this->increaseToMin($this->_personnel_number, self::LOGIN_LENGTH);
    }

    /**
     * @param string $personnel_number
     */
    public function setpersonnel_number($personnel_number): void
    {
        $this->_personnel_number = $personnel_number;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->increaseToMin($this->_password, self::PASSWORD_LENGTH);
    }

    /**
     * @param string $password
     */
    public function setPassword($password): void
    {
        $this->_password = $password;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->_level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level): void
    {
        $this->_level = $level;
    }

    /**
     * @return string
     */
    public function getUpdated_at()
    {
        return date("d-M-y", (int)$this->_updated_at);
    }

    /**
     * @param string $updated_at
     */
    public function setUpdated_at($updated_at): void
    {
        $this->_updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getNull(): string
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->transliteration::run($this->_department);
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department): void
    {
        $this->_department = $department;
    }

    /**
     * @return mixed
     */
    public function getInactive()
    {
        return $this->_inactive;
    }

    /**
     * @param mixed $inactive
     */
    public function setInactive($inactive): void
    {
        $this->_inactive = $inactive;
    }

    /**
     * @inheritDoc
     */
    public static function getFileName()
    {
        return 'new.txt';
    }
}