<?php


namespace app\models;


use Yii;
use yii\base\Exception;
use yii\base\Model;

class Export extends Model
{
    /**
     * @var Row
     */
    protected $_row;

    /**
     * @var string
     */
    protected $_data;

    public function __construct($row, $config = [])
    {
        $this->_row = $row;
        parent::__construct($config);
    }

    /**
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function save(array $data)
    {
        $rows = [];
        $export = $this->_row;
        foreach ($data as $value) {
            /** @var Row $class */
            $class = new $export();
            $class->attributes = $value;
            $rows[] = $class->toString();
        }
        if ($superuser = $class::getSuperuser()) {
            $rows[] = $superuser->toString();
        }
        $this->_data = join($export::$row_separator, $rows);
        return $this;
    }

    public function send()
    {
        $file = Yii::$app->response->sendContentAsFile($this->_data, $this->_row::getFileName());
        $file->send();
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->_data;
    }
}