<?php


namespace app\controllers;


use app\models\Employees;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    /**
     * @var Employees
     */
    public $modelClass = 'app\models\Employees';

    public $createScenario = Employees::SCENARIO_CREATE;

    public $deleteScenario = Employees::SCENARIO_DELETE;

    public function actions()
    {
        $actions = parent::actions();
        $actions['delete']['class'] = 'app\models\employees\DeleteAction';
        $actions['delete']['scenario'] = $this->deleteScenario;
        return $actions;
    }
}