<?php


namespace app\controllers;

use app\models\Export;
use yii\base\InvalidConfigException;
use yii\rest\Controller;

abstract class ExportController extends Controller
{
    public $exportModel;

    protected function verbs()
    {
        return [
            'index' => ['GET'],
        ];
    }

    public function init()
    {
        if ($this->exportModel === null) {
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }
    }

    public function actionIndex()
    {
        $data = $this->getData();
        $rows = new Export($this->exportModel);
        $rows->save($data)
            ->send();
        return true;
    }

    abstract protected function getData();
}