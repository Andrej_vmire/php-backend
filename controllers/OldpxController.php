<?php


namespace app\controllers;


use app\models\OldPXRow;
use app\traits\GetDataTrait;

class OldpxController extends ExportController
{
    use GetDataTrait;

    public $exportModel = OldPXRow::class;
}