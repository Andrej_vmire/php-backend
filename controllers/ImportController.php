<?php


namespace app\controllers;


use app\models\Employees;
use app\models\ImportUsers;
use Throwable;
use Yii;
use yii\rest\Controller;

class ImportController extends Controller
{
    protected function verbs()
    {
        return [
            'index' => ['POST']
        ];
    }

    public function actionIndex()
    {
        $model = new ImportUsers();
        $array = $model->importing();
        $result = [];
        foreach ($array as $key => $value) {
            $employee = new Employees();
            $employee->scenario = Employees::SCENARIO_CREATE;
            $arrayMerge = array_merge(
                Yii::$app->request->post(), $value
            );
            $employee->attributes = $arrayMerge;
            try {
                if ($employee->insert()) {
                    $result['success'][] = $employee->personnel_number;
                } else {
                    foreach ($employee->getErrors() as $ekey => $error) {
                        $result['no valid'][$ekey][] = $error;
                    }
                }
            } catch (Throwable $e) {
                $result['undone'][$e->getCode()][] = $employee->personnel_number;
            }
        }
        return $result;
    }
}