<?php


namespace app\controllers;


use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;

class AccessController extends ActiveController
{
    public $modelClass = "app\models\Access";
}