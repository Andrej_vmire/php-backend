<?php


namespace app\controllers;


use app\models\ExportToFile;
use app\models\HiScanRow;
use app\traits\GetDataTrait;

class HiscanController extends ExportController
{
    use GetDataTrait;

    public $exportModel = HiScanRow::class;

    public function actionIndex()
    {
        $date = $this->getData();
        $rows = new ExportToFile($this->exportModel);
        $rows->save($date)
            ->send();
        return true;
    }
}