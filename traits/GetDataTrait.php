<?php


namespace app\traits;


use app\models\Employees;

trait GetDataTrait
{
    protected function getData()
    {
        return Employees::find()
            ->select(['e.*', 'd.name AS department', 'a.name AS access', 'a.level'])
            ->from('employees e')
            ->withDepartment()
            ->withAccess()
            ->exceptInacive()
            ->asArray()
            ->all();
    }
}