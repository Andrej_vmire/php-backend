<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%access}}`.
 */
class m191019_121714_create_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%access}}', [
            'id' => $this->primaryKey(),
            'level' => $this->integer(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%access}}');
    }
}
